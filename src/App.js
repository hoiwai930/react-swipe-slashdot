import React,{useState,useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';


// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

import SwiperCore, {Keyboard} from 'swiper';

// Import Swiper styles
import 'swiper/swiper.scss';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  content: {
    fontSize: 16,
  },
  pos: {
    marginBottom: 12,
  },
});


function Story(props) {
  const classes = useStyles();
  return (
        <Card className={classes.root}>
          <CardContent>
            <Typography variant="h5" component="h2">
              {props.title}
            </Typography>
            <Typography className={classes.content} variant="body2" component="p">
              {props.content}
            </Typography>
          </CardContent>
        </Card>
  );
};

function App() {
  const [data,setData]=useState([]);
  const getData=()=>{
    fetch('https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Frss.slashdot.org%2FSlashdot%2FslashdotMain'
    ,{
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       }
    }
    )
      .then(function(response){
        console.log(response)
        return response.json();
      })
      .then(function(myJson) {
        console.log(myJson);
        setData(myJson)
      });
  }
  useEffect(()=>{
    getData()
  },[])

  return (
    <Swiper
      spaceBetween={50}
      slidesPerView={1}
      onSlideChange={() => console.log('slide change')}
      keyboard={{
        enabled: true,
        onyInViewport: false
      }}
      pagination={{
        el: '.swiper-pagination',
        clickable: true,
      }}
      navigation={{
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      }}
      onSwiper={(swiper) => window.swiper=swiper}
    >
      {data.items && data.items.map((story, index) => (
        <SwiperSlide key={index}>
          <Story title={story.title} content={story.content.match(/(.*?)<p>/s)[1]}>
          </Story>
        </SwiperSlide>
       ))}
    </Swiper>
  );
};

SwiperCore.use([Keyboard]);

export default App;
